import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'UI/app_bar.dart';
import '2048/route.dart';
import '2048/logic.dart';

void main() => SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
    .then((_) {
      runApp(new MyApp());
    });

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '2048',
      theme: ThemeData(
          // primarySwatch: Colors.indigo,
          ),
      home: Game(),
    );
  }
}

class Game extends StatelessWidget {
  Game({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final gameRoute = Game2048(
      logic: Logic(),
    );
    final appBar = createAppBar(
      barHeight: 36.0,
      gameButtons: gameRoute.view.buttons,
    );

    return Scaffold(
      appBar: appBar,
      body: gameRoute,
      backgroundColor: Colors.indigo[200],
    );
  }
}
