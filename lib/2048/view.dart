import 'package:flutter/material.dart';
import 'package:swipedetector/swipedetector.dart';

import 'tile.dart';

class View extends StatefulWidget {
  final logic;
  final List<Widget> buttons = [];

  View({Key key, this.logic}) : super(key: key);

  _ViewState createState() => _ViewState();
}

class _ViewState extends State<View> {
  @override
  void initState() {
    super.initState();
    widget.buttons.clear();
    widget.buttons.add(createUndoButton());
    widget.buttons.add(createRestartButton());
    widget.logic.putValues(2);
  }

  createUndoButton([iconSize = 36.0]) {
    return IconButton(
      icon: Icon(
        Icons.undo,
        size: iconSize,
      ),
      color: Colors.indigo[400],
      splashColor: Colors.indigo,
      onPressed: () {
        setState(() {
          widget.logic.undo();
        });
      },
    ); // gameField.undo()
  }

  createRestartButton([iconSise = 36.0]) {
    return IconButton(
      icon: Icon(
        Icons.refresh,
        size: iconSise,
      ),
      color: Colors.indigo[400],
      splashColor: Colors.indigo,
      onPressed: () {
        setState(() {
          widget.logic.restart();
        });
      },
    );
  }

  void push(String direction) {
    if (widget.logic.push(direction)) {
      setState(() {
        widget.logic.putValues();
        print("pushed $direction");
      });
    } else
      print("something went wrong in view");
  }

  Widget buildGrid(List<int> field, int size) {
    final rows = <Widget>[];
    for (int row = 0; row < size; row++)
      rows.add(
        Expanded(
          child: Row(
            children: field
                .skip(row * size)
                .take(size)
                .map(
                  (item) => Expanded(
                        child: Tile(item),
                      ),
                )
                .toList(),
          ),
        ),
      );
    return Container(
      padding: EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: rows,
      ),
      height: 500.0,
      width: 500.0,
      // color: Colors.red,
    );
  }

  @override
  Widget build(BuildContext context) {
    return 
    Expanded(
        child: Container(
          
          child: SwipeDetector(
            child: FittedBox(
                child: buildGrid(widget.logic.field, widget.logic.size)),
            onSwipeDown: () => push('down'),
            onSwipeLeft: () => push('left'),
            onSwipeRight: () => push('right'),
            onSwipeUp: () => push('up'),
            swipeConfiguration: SwipeConfiguration(
              verticalSwipeMinVelocity: 100.0,
              horizontalSwipeMinVelocity: 100.0,
              verticalSwipeMinDisplacement: 42.0,
              horizontalSwipeMinDisplacement: 42.0,
              verticalSwipeMaxWidthThreshold: 140.0,
              horizontalSwipeMaxHeightThreshold: 140.0,
            ),
          ),
      ),
    );
  }
}
