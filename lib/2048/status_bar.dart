import 'package:flutter/material.dart';

const _fontSize = 24.0;

class StatusBar extends StatefulWidget {
  final logic;
  StatusBar({this.logic});

  _StatusBarState createState() => _StatusBarState();
}

class _StatusBarState extends State<StatusBar> {
  int highscore;
  int currentScore;
  @override
  void initState() {
    super.initState();
    highscore = widget.logic.highScore;
    currentScore = widget.logic.currentScore;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 8.0,
        horizontal: 16.0,
      ),
      child: Row(
        children: [
          Expanded(
            child: Text(
              "Current score: $currentScore",
              textAlign: TextAlign.start,
              style: TextStyle(
                color: Colors.indigo[400],
                fontSize: _fontSize,
              ),
            ),
          ),
          Expanded(
            child: Text(
              "Highscore: $highscore",
              textAlign: TextAlign.end,
              style: TextStyle(
                color: Colors.indigo[400],
                fontSize: _fontSize,
              ),
            ),
          ),
        ],
      ),
      // color: Colors.indigo[200],
    );
  }
}
