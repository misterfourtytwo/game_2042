import 'package:flutter/material.dart';

final _colors = {
  0: Colors.indigo[100],
  2: Colors.green[200],
  4: Colors.green[400],
  8: Colors.blue[200],
  16: Colors.blue[400],
  32: Colors.blue[600],
  64: Colors.indigo[300],
  128: Colors.indigoAccent,
  256: Colors.indigo[600],
  512: Colors.purple[200],
  1024: Colors.purple[400],
  2048: Colors.purple,
  4096: Colors.pink[400],
  8192: Colors.pinkAccent[400],
};

class Tile extends StatelessWidget {
  final int value; // current tile value
  final double size;
  Tile(this.value, {this.size});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          width: 3.0,
          color: Colors.indigo[800],
        ),
        borderRadius: BorderRadius.circular(20.0),
        color: _colors[value],
        shape: BoxShape.rectangle,
      ),
      margin: EdgeInsets.all(2),
      height: this.size ?? null,
      width: this.size ?? null,
      child: value == 0
          ? null
          : Center(
              child: Text(value.toString(),
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 42.0,
                  ))),
    );
  }
}
