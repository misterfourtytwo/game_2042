import 'dart:math';

class Logic {
  final size;
  final _random = Random();

  List<int> field = [], _prevField;
  int get _freeCells => field.where((e) => e == 0).length;

  int highScore, currentScore, _prevScore;

  /// helper that returns empty game field
  List<int> emptyField() {
    return List<int>.generate(
      size * size,
      (_) => 0,
      growable: false,
    );
  }

  /// changes game state to before the last move
  bool undo() {
    if (_prevField == null) {
      print("cannot undo");
      return false;
    }
    print("UNDO");

    field = _prevField;
    _prevField = null;
    currentScore = _prevScore;
    _prevScore = null;
    return true;
  }

  /// nullifies game state and score
  void restart() {
    print("RESTART");
    highScore = max(currentScore, highScore);

    field = emptyField();
    putValues(2);
    _prevField = null;
    currentScore = _prevScore = 0;
  }

  /// helper function that pushes all values in specified
  /// direction to the appropriate grid border,
  /// returns false when no value has been moved
  bool compress(String direction) {
    int step;
    Function canStep;
    bool moved = false;

    switch (direction) {
      case 'left':
        step = 1;
        canStep = (int i) => i % size != 0;
        break;
      case 'up':
        step = size;
        canStep = (int i) => i >= size;
        break;
      case 'down':
        step = -size;
        canStep = (int i) => i / size < size - 1;
        break;
      case 'right':
        step = -1;
        canStep = (int i) => i % size != size - 1;
        break;
      default:
        print("something went wrong");
        return false;
    }

    int i = 0, to = field.length, inc = 1;

    // for those swipes, that need backward traversal,
    // e.g. step is less than zero
    if (step < 0) {
      i = to - 1;
      to = -1;
      inc = -1;
    }
    // for (i = 0; i != _field.length; i++)
    // for (i = _field.length-1; i != -1; i--)
    for (; i != to; i += inc) {
      if (field[i] == 0) continue;
      var j = i;
      while (canStep(j) && field[j - step] == 0) {
        field[j - step] = field[j];
        field[j] = 0;
        j -= step;
        moved = true;
      }
    }
    return moved;
  }

  /// function that performs moving and merging of
  /// values in grid in specified direction
  /// returns false when no value has been moved or merged
  bool push(String direction) {
    int step = 1;
    var canStep;
    bool moved = false;

    switch (direction) {
      case 'left':
        step = 1;
        canStep = (int i) => i % size != 0;
        break;
      case 'up':
        step = size;
        canStep = (int i) => i >= size;
        break;
      case 'down':
        step = -size;
        canStep = (int i) => i / size < size - 1;
        break;
      case 'right':
        step = -1;
        canStep = (int i) => i % size != size - 1;
        break;
      default:
        print("something went wrong");
        return moved;
    }

    print("swipe $direction");

    var tmpPrevField = field.toList(growable: false);
    var tmpPrevScore = currentScore;
    moved |= compress(direction);

    int i = 0, to = field.length, inc = 1;

    if (step < 0) {
      i = to - 1;
      to = -1;
      inc = -1;
    }

    for (; i != to; i += inc)
      if (field[i] != 0 && canStep(i) && field[i - step] == field[i]) {
        field[i - step] = field[i] * 2;
        currentScore += field[i - step];
        field[i] = 0;
        moved = true;
      }
    moved |= compress(direction);
    if (moved) {
      _prevField = tmpPrevField;
      _prevScore = tmpPrevScore;
    }
    return moved;
  }

  /// put specified number of values into the field
  /// returns false if can not perform the action
  bool putValues([int cnt = 1]) {
    if (_freeCells < cnt) {
      print("Cannot create new tile, not space left");
      return false;
    }

    while (cnt > 0)
      for (int cellNum = _random.nextInt(_freeCells) + 1, i = 0;
          i < field.length;
          i++)
        if ((field[i] == 0) && (--cellNum == 0)) {
          field[i] = (2 << _random.nextInt(2));
          currentScore += field[i];
          cnt--;
          break;
        }
    return true;
  }

  Logic({this.size = 4}) {
    field = emptyField();
    currentScore = _prevScore = 0;
    highScore = _random.nextInt(100500);
  }
}
