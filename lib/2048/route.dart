import 'package:flutter/material.dart';

import 'status_bar.dart';
import 'view.dart';
import 'logic.dart';

class Game2048 extends StatelessWidget {
  final Logic logic;
  final View view;
  final StatusBar statusBar;

  Game2048({Key key, this.logic})
      : view = View(
          logic: logic,
        ),
        statusBar = StatusBar(
          logic: logic,
        );

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        statusBar,
        view,
      ],
    );
  }
}
