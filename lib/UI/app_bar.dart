import 'package:flutter/material.dart';

Widget createAppBar({
  double barHeight,
  List<Widget> gameButtons,
}) {
  return AppBar(
    title: Text(
      '2048',
      style: TextStyle(
        color: Colors.indigo[400],
        fontSize: barHeight,
      ),
    ),
    centerTitle: true,

    elevation: 1.0,
    backgroundColor: Colors.indigo[200],

    leading: IconButton(
      icon: Icon(
        Icons.close,
        size: barHeight,
      ),
      color: Colors.indigo[400],
      splashColor: Colors.indigo,
      onPressed: () => print("Pressed exit"),
    ), // close app

    actions: gameButtons,
  );
}
